/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import com.mycompany.databaseproject.dao.UserDao;
import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.databaseproject.model.User;
import com.mycompany.databaseproject.service.UserService;

/**
 *
 * @author user
 */
public class TestUserDao {

    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("mark", "mark");
        if(user!=null){
            System.out.println("welcome user : "+user.getName());
        }else{
            System.out.println("error");
        }
    }
}
